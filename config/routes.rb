Rails.application.routes.draw do
  get 'sessions/new'

  root "index_pages#home"
  
  get "about" => "index_pages#about"
  get "signup" => "users#new"
  get 'login' => 'sessions#new'
  
  post 'login' => 'sessions#create'
  post 'vote' => 'posts#voting'
  
  delete 'logout' => 'sessions#destroy'
  resources :users
  resources :posts
end
