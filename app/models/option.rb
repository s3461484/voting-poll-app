class Option < ActiveRecord::Base
  belongs_to :post
  before_create { self.name = '' }
  before_create { self.count = 0 }
  
  validates :value,  presence: true, length: { maximum: 255 }
  default_scope -> { order(count: :desc) }
end
