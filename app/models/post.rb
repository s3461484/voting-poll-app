class Post < ActiveRecord::Base
  belongs_to :user
  has_many :options, dependent: :destroy
  accepts_nested_attributes_for :options, reject_if: :all_blank, allow_destroy: true, update_only: true
  
  validates :title,  presence: true, length: { maximum: 255 }
  validates :vote_limit, :numericality => { :greater_than => 0 }
  default_scope -> { order(created_at: :desc) }
  
  validate :start_date_before_end_date
  validate :must_have_at_least_2_options
  validate :vote_limit_must_less_than_number_of_options
  
  def start_date_before_end_date
    if !start_date.nil? && !close_date.nil?
      if start_date > close_date
        errors.add(:start_date, "Must Occur Before End Date")
      end
    end
  end
  
  def must_have_at_least_2_options
    if self.options.length < 2
      errors.add(:options, "Not enough option")
    end
  end
  
  def vote_limit_must_less_than_number_of_options
    if self.options.length <= self.vote_limit
      errors.add(:vote_limit, "Vote limit can not be equal to or greater than number of options")
    end
  end
end
