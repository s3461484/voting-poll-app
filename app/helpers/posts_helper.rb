module PostsHelper
  def user_name(opt)
      ids = opt.name.to_s.split(',')
      names = Array.new
      ids.each do |id|
        user = User.find(id)
        names.push(user.name)
      end
      return names
    end
end
