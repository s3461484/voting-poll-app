class PostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :new]
  
  def index
    @posts = Post.paginate(page: params[:page])
  end
  
  def show
    @post = Post.find(params[:id])
    @options = @post.options
    @is_voted = false
  end
  
  def new
    @post = Post.new
    @post.options.build
  end
  
  def create
    @post = Post.new(post_params)
    if @post.save
      redirect_to @post
      flash[:success] = "Post created successfully"
    else
      render 'new'
    end
  end
  
  def voting
    po = Post.find(params[:options][:post_id])
    if !params[:options_id]
      if params[:option_id].length <= po.vote_limit
        params[:option_id].each do |o|
          opt = Option.find(o.to_i)
          opt.update_attributes(count: (opt.count + 1).to_s)
          opt.update_attributes(name: opt.name + current_user.id.to_s + ",")
        end
        flash[:success] = "Voted successfully"
        redirect_to po
      else
        flash[:danger] = "Vote limit exceeded" 
        redirect_to po
      end
    else
      flash[:danger] = "No vote committed" 
      redirect_to po
    end
  end
  
  def update
    @post = Post.find(params[:id])
    if @post.update_attributes(post_params)
      flash[:success] = "Update succeeded"
      redirect_to @post
    else
      render 'show'
    end
  end
  
  private
    def post_params
      params.require(:post).permit(:title, :description, :enable_adding, :allow_anonymous_voting,
                                   :close_date, :start_date, :background_img, :user_id, :vote_limit,
                                   options_attributes: [:id, :value, :description, :img, :count, :name, :_destroy])
    end
    
    # def is_voted (po)
    #   options = po.options
    #   options.each do |option|
    #     if option.name.split(',').include? current_user.id.to_s
    #       return true
    #     end
    #   end
    #   return false
    # end
end
