class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.references :user, index: true
      t.string :title
      t.text :description
      t.boolean :enable_adding, default: false
      t.boolean :allow_anonymous_voting, default: false
      t.datetime :close_date
      t.datetime :start_date
      t.string :background_img
      
      t.timestamps null: false
    end
  end
end
