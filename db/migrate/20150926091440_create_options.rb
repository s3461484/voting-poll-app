class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.references :post, index: true
      t.string :value
      t.text :description
      t.integer :count
      t.string :name
      t.string :img

      t.timestamps null: false
    end
  end
end
