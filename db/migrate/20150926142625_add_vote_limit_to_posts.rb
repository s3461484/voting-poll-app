class AddVoteLimitToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :vote_limit, :integer
  end
end
